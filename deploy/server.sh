# Pull code
cd nodejs
# git clone git@gitlab.com:phungthetrung11t2/gitlab_cicd.git
cd gitlab_cicd
git checkout main
git pull origin main

# Build and deploy
yarn install
pm2 start hello.js
